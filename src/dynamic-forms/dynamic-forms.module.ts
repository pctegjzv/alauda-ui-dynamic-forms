import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { ReactiveFormsModule } from '@angular/forms';
import { DynamicFormComponent } from './dynamic-form.component';
import { DynamicFormService } from './dynamic-form.service';
import { DynamicFormDefDirective } from './dynamic-form-def.directive';
import { DynamicFormOutletDirective } from './dynamic-form-outlet.directive';

@NgModule({
  declarations: [
    DynamicFormComponent,
    DynamicFormOutletDirective,
    DynamicFormDefDirective
  ],
  exports: [DynamicFormComponent, DynamicFormDefDirective],
  imports: [CommonModule, ReactiveFormsModule],
  providers: [DynamicFormService]
})
export class DynamicFormsModule {}
