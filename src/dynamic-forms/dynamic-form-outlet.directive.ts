import { Directive, ViewContainerRef, TemplateRef } from '@angular/core';

@Directive({
  selector: '[auiDynamicFormOutlet]'
})
export class DynamicFormOutletDirective {
  constructor(public viewContainer: ViewContainerRef) {}
}
