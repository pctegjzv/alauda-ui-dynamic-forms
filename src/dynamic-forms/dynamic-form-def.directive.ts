import { Directive, ViewContainerRef, TemplateRef } from '@angular/core';

@Directive({
  selector: '[auiDynamicFormDef]'
})
export class DynamicFormDefDirective {
  constructor(public template: TemplateRef<any>) {}
}
