import { Observable } from 'rxjs';

export interface Dictionary<T> {
  [name: string]: T;
}

export interface NameValuePair<T> {
  name: string;
  value: T;
}

export interface LocalizedString {
  ['zh-CN']: string;
  en: string;
}

export interface AllCondition {
  all: NameValuePair<any>[];
}

export interface AnyCondition {
  any: NameValuePair<any>[];
}

export interface RelationDefine {
  when: NameValuePair<any> | AllCondition | AnyCondition;
  action: string;
}

export interface FieldDefine {
  name: string;
  schema: {
    type: string; // 'int' | 'string' | 'number' | 'boolean' | 'map' | 'array' | 'alauda.io/*'
    items?: {
      type: string;
    };
    enum?: any[];
  };
  required: boolean;
  default: any;
  validation: {
    pattern?: string;
    maxLength?: number;
  };
  display: {
    type: string;
    args?: any;
    related?: string; // TODO: { [field: string]: string } will be considered.
    name: LocalizedString;
    description: LocalizedString;
  };
  relation: RelationDefine[];
}

export interface GroupDefine {
  displayName: LocalizedString;
  items: FieldDefine[];
  relation: RelationDefine[];
}

export interface NormalizedGroupDefine extends GroupDefine {
  hidden: (values: Dictionary<FormControlState>) => boolean;
}

export interface NormalizedFieldDefine extends FieldDefine {
  hidden: (state: Dictionary<FormControlState>) => boolean;
}

export interface NormalizedDefine {
  groups: Dictionary<NormalizedGroupDefine>;
  controls: Dictionary<NormalizedFieldDefine>;
  dependencies: Dictionary<string[]>;
}

export interface FormControlState {
  name: string;
  required?: boolean;
  hidden?: boolean;
  options?: any[];
  pending?: boolean;
  value: any;
  relatedValue?: any;
  error?: any;
}

export interface FormGroupState {
  name: string;
  hidden?: boolean;
}

export interface FormState {
  controls: Dictionary<FormControlState>;
  groups: Dictionary<FormGroupState>;
  pending?: boolean;
}

export interface ControlMapper {
  [type: string]: {
    optionsResolver: OptionsResolver;
    valueOf?: (item: any) => any;
    displayName?: (item: any) => any;
  };
}

export type OptionsResolver = (related: any, state: FormState, args: any) => Observable<any[]>;
