import { Injectable, InjectionToken, Inject } from '@angular/core';
import { FormGroup, FormControl } from '@angular/forms';
import { Observable, of, Subject, Subscription, forkJoin, merge } from 'rxjs';
import {
  map,
  switchMap,
  concatMap,
  tap,
  startWith,
  publishReplay,
  refCount,
  catchError,
  withLatestFrom
} from 'rxjs/operators';
import { mapValues, get, flatMap, union } from 'lodash';
import {
  GroupDefine,
  FormState,
  NormalizedDefine,
  Dictionary,
  NormalizedGroupDefine,
  RelationDefine,
  NameValuePair,
  NormalizedFieldDefine,
  OptionsResolver,
  ControlMapper
} from './types';
import { combineReducer } from './utils';

@Injectable()
export class DynamicFormService {
  constructor() {}

  buildForm(
    define: GroupDefine[],
    model: any,
    controlMapper: ControlMapper,
  ): Observable<{
    state: FormState;
    group: FormGroup;
  }> {
    const normalizeDefine = this.normalizeDefine(define);
    const valueChanges$ = new Subject<any>();
    let valueChangesSubscription: Subscription = null;

    const getControlConfig = (field: NormalizedFieldDefine) => {
      if (!field.display) {
        return null;
      }
      return controlMapper[field.display.type] || null;
    };

    const initialForm$ = of(this.getInitialState(normalizeDefine, model)).pipe(
      concatMap(state =>
        this.loadOptions(normalizeDefine, state, getControlConfig, true)
      ),
      publishReplay(1),
      refCount()
    );

    const stateProxy$ = new Subject<FormState>();

    const stateOnValueChanges$ = valueChanges$.pipe(
      withLatestFrom(stateProxy$),
      switchMap(([changes, state]) => {
        return this.loadOptions(
          normalizeDefine,
          this.resetRelated(normalizeDefine, state, changes),
          getControlConfig
        );
      })
    );

    return merge(initialForm$, stateOnValueChanges$).pipe(
      map(state => ({ state, group: this.toFormGroup(state) })),
      tap(({ state, group }) => {
        stateProxy$.next(state);
        if (valueChangesSubscription) {
          valueChangesSubscription.unsubscribe();
        }
        valueChangesSubscription = group.valueChanges.subscribe(valueChanges$);
      }),
      publishReplay(1),
      refCount()
    );
  }

  private resetRelated(
    define: NormalizedDefine,
    originalState: FormState,
    changes: any
  ) {
    const relatedFields = union(
      ...Object.keys(changes).map(
        key =>
          changes[key] !== originalState.controls[key].value &&
          define.dependencies[key]
      )
    );

    const changedControls = Object.keys(originalState.controls).reduce(
      (controls, key) => {
        return {
          ...controls,
          [key]: {
            ...controls[key],
            value: relatedFields.includes(key)
              ? null
              : get(changes, key) || null
          }
        };
      },
      originalState.controls
    );
    return this.checkHiddenAndRequiredChange(define, {
      ...originalState,
      controls: changedControls
    });
  }

  private loadOptions(
    define: NormalizedDefine,
    state: FormState,
    getControlConfig: (
      name: NormalizedFieldDefine
    ) => { optionsResolver: OptionsResolver },
    updateFormPending = false
  ): Observable<FormState> {
    const pendings = Object.keys(state.controls).filter(key =>
      this.optionsExpiredOrNotInitialized(
        define.controls[key],
        state,
        getControlConfig
      )
    );
    if (!pendings.length) {
      return of(updateFormPending ? { ...state, pending: false } : state);
    }

    const controlsPendingStart = pendings.reduce((controls, pending) => {
      return {
        ...controls,
        [pending]: {
          ...controls[pending],
          pending: true
        }
      };
    }, state.controls);

    return forkJoin(
      pendings.map(pending => {
        const field = define.controls[pending];
        const controlConfig = getControlConfig(field);
        const relatedValue =
          field.display && field.display.related
            ? state.controls[field.display.related].value
            : null;

        return controlConfig
          .optionsResolver(relatedValue, state, field.display.args)
          .pipe(
            map(result => ({ options: result, error: null, relatedValue })),
            catchError((error: any) =>
              of({ options: null, error, relatedValue })
            )
          );
      })
    ).pipe(
      map(results => {
        const controlsPendingEnd = pendings.reduce(
          (controls, pending, index) => {
            return {
              ...controls,
              [pending]: {
                ...controls[pending],
                options: results[index].options || [],
                error: results[index].error || null,
                relatedValue: results[index].relatedValue,
                pending: false
              }
            };
          },
          state.controls
        );

        return {
          ...state,
          controls: controlsPendingEnd,
          pending: updateFormPending ? false : state.pending
        };
      }),
      startWith({
        ...state,
        controls: controlsPendingStart
      })
    );
  }

  private normalizeDefine(template: GroupDefine[]): NormalizedDefine {
    const reducer = combineReducer({
      groups: groupsReducer,
      controls: controlsReducer,
      dependencies: dependenciesReducer
    });

    const { groups, controls, dependencies } = template.reduce(reducer, {
      groups: {} as Dictionary<NormalizedGroupDefine>,
      controls: {} as Dictionary<NormalizedFieldDefine>,
      dependencies: {} as Dictionary<string[]>
    });

    // flatten dependencies for quick reset when value change
    const flatDependencies = (key: string): string[] => {
      const flattened = [
        ...(dependencies[key] || []),
        ...flatMap(dependencies[key], sub => flatDependencies(sub))
      ];

      if (flattened.includes(key)) {
        throw Error(`"${key}" has circular dependency`);
      }

      return flattened;
    };

    return {
      groups,
      controls,
      dependencies: Object.keys(dependencies).reduce(
        (accum, key) => ({ ...accum, [key]: flatDependencies(key) }),
        {}
      )
    };
  }

  private toFormGroup(state: FormState): FormGroup {
    const group = Object.keys(state.controls).reduce((accum, key) => {
      return {
        ...accum,
        [key]: new FormControl(state.controls[key].value)
      };
    }, {});

    return new FormGroup(group);
  }

  private optionsExpiredOrNotInitialized(
    field: NormalizedFieldDefine,
    state: FormState,
    getControlConfig: (
      name: NormalizedFieldDefine
    ) => { optionsResolver: OptionsResolver }
  ): boolean {
    if (!field.display) {
      return false;
    }

    const control = state.controls[field.name];
    const controlConfig = getControlConfig(field);

    if (!controlConfig || !controlConfig.optionsResolver) {
      return false;
    }
    if (!control.options || !control.options.length) {
      return true;
    }
    if (!field.display.related) {
      return false;
    }

    return control.relatedValue !== state.controls[field.display.related].value;
  }

  private getInitialState(define: NormalizedDefine, model: any) {
    const { groups, controls } = this.toFormState(define, model);

    return this.checkHiddenAndRequiredChange(define, {
      groups,
      controls,
      pending: true
    });
  }

  private toFormState(define: NormalizedDefine, model: any): FormState {
    const controls = mapValues(define.controls, field => ({
      name: field.name,
      value: get(model, field.name) || field.default || null,
      options: field.schema.enum || []
    }));

    const groups = mapValues(define.groups, group => ({
      name: group.displayName.en
    }));

    return {
      controls,
      groups
    };
  }

  private checkHiddenAndRequiredChange(
    define: NormalizedDefine,
    state: FormState
  ): FormState {
    const controls = mapValues(state.controls, control => ({
      ...control,
      required:
        define.controls[control.name] &&
        !define.controls[control.name].hidden(state.controls),
      hidden: define.controls[control.name].hidden(state.controls)
    }));

    const groups = mapValues(state.groups, group => ({
      ...group,
      hidden: define.groups[group.name].hidden(state.controls)
    }));

    return {
      ...state,
      controls,
      groups
    };
  }
}

function groupsReducer(
  groups: Dictionary<NormalizedGroupDefine>,
  group: GroupDefine
): Dictionary<NormalizedGroupDefine> {
  const orignal = groups || {};

  return {
    ...orignal,
    [group.displayName.en]: {
      ...group,
      hidden: compileRelations(group.relation)
    }
  };
}

function controlsReducer(
  controls: Dictionary<NormalizedFieldDefine>,
  group: GroupDefine
): Dictionary<NormalizedFieldDefine> {
  const orignal = controls || {};

  const groupControls = group.items.reduce(
    (accum, item) => ({
      ...accum,
      [item.name]: {
        ...item,
        hidden: compileRelations(item.relation)
      }
    }),
    {} as Dictionary<NormalizedFieldDefine>
  );

  return {
    ...orignal,
    ...groupControls
  };
}

// TODO: { [field: string]: string } will be considered.
function dependenciesReducer(
  dependencies: Dictionary<string[]>,
  group: GroupDefine
): Dictionary<string[]> {
  const orignal = dependencies || ({} as Dictionary<string[]>);

  return group.items.reduce((accum, item) => {
    if (!item.display || !item.display.related) {
      return accum;
    }

    const existed = accum[item.display.related] || [];

    return {
      ...accum,
      [item.display.related]: [...existed, item.name]
    };
  }, orignal);
}

// TODO: only show hide available
function compileRelations(relations: RelationDefine[]) {
  const availableRelation = (relations || []).find(item =>
    ['show', 'hide'].includes(item.action)
  );

  if (!availableRelation) {
    return () => false;
  }
  if (availableRelation.action === 'show') {
    return (values: any) =>
      !whenExpression(availableRelation.when).call(null, values);
  }

  return whenExpression(availableRelation.when);
}

function whenExpression(when): (state: FormState) => boolean {
  if (!when) {
    return () => false;
  }
  if (when.name) {
    return (state: FormState) => state[when.name].value === when.value;
  }
  if (when.all) {
    return (state: FormState) =>
      ((when.all as NameValuePair<any>[]) || []).every(
        item => state[item.name].value === item.value
      );
  }
  if (when.any) {
    return (state: FormState) =>
      ((when.any as NameValuePair<any>[]) || []).some(
        item => state[item.name].value === item.value
      );
  }
}
