import {
  Component,
  Input,
  OnChanges,
  OnInit,
  ChangeDetectionStrategy,
  ViewEncapsulation,
  EmbeddedViewRef,
  ViewChild,
  ContentChild,
  AfterContentChecked
} from '@angular/core';
import { FormGroup } from '@angular/forms';
import { GroupDefine, FormState, ControlMapper } from './types';
import { DynamicFormService } from './dynamic-form.service';
import { DynamicFormDefDirective } from './dynamic-form-def.directive';
import { DynamicFormOutletDirective } from './dynamic-form-outlet.directive';

@Component({
  selector: 'aui-dynamic-form',
  templateUrl: 'dynamic-form.component.html',
  changeDetection: ChangeDetectionStrategy.OnPush,
  encapsulation: ViewEncapsulation.None
})
export class DynamicFormComponent
  implements OnInit, OnChanges, AfterContentChecked {
  @Input()
  template: GroupDefine[];

  @Input()
  model: any;

  @Input()
  controlMapper: ControlMapper;

  private initialized = false;

  @ViewChild(DynamicFormOutletDirective)
  private formOutlet: DynamicFormOutletDirective;

  @ContentChild(DynamicFormDefDirective)
  private formDef: DynamicFormDefDirective;

  private context: {
    $implicit: FormGroup;
    state: FormState;
  } = {
    $implicit: null,
    state: null
  };

  private view: EmbeddedViewRef<any>;

  constructor(private dynamicForm: DynamicFormService) {}

  ngOnInit() {
    this.rebuildForm();
    this.initialized = true;
  }

  ngOnChanges() {
    if (this.initialized) {
      this.rebuildForm();
    }
  }

  private rebuildForm() {
    this.dynamicForm
      .buildForm(this.template, this.model, this.controlMapper)
      .subscribe(({ state, group }) => {
        this.context.$implicit = group;
        this.context.state = state;
        if (this.view) {
          this.view.markForCheck();
        }
      });
  }

  ngAfterContentChecked() {
    if (!this.view) {
      this.view = this.formOutlet.viewContainer.createEmbeddedView(
        this.formDef.template,
        this.context
      );
    }
  }
}
