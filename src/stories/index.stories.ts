import { storiesOf } from '@storybook/angular';
import { CommonModule } from '@angular/common';
import { ReactiveFormsModule } from '@angular/forms';
import { HttpClientModule } from '@angular/common/http';
import { DynamicFormsModule } from '../dynamic-forms';
import { DemoComponent } from './demo.component';
import { forwardRef } from '@angular/core';

const imports = [
  CommonModule,
  HttpClientModule,
  ReactiveFormsModule,
  DynamicFormsModule
];

storiesOf('Dynamic Form', module).add('default', () => {
  return {
    moduleMetadata: {
      imports
    },
    component: DemoComponent
  };
});
