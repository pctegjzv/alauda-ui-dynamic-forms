import { Component, ChangeDetectionStrategy, ViewChild } from '@angular/core';
import { GroupDefine, FieldDefine, ControlMapper } from '../dynamic-forms';
import { NgForm } from '@angular/forms';
import { of } from 'rxjs';
import { delay } from 'rxjs/operators';

@Component({
  templateUrl: 'demo.component.html',
  changeDetection: ChangeDetectionStrategy.OnPush
})
export class DemoComponent {
  title = 'app';
  @ViewChild('ngForm')
  ngForm: NgForm;

  model = {
    abc: '123',
    efg: '456'
  };

  template: GroupDefine[] = [
    {
      displayName: {
        en: 'abc',
        'zh-CN': '第一组'
      },
      relation: null,
      items: [
        {
          name: 'abc',
          schema: {
            type: 'string'
          },
          required: false,
          validation: {},
          default: null,
          display: {
            type: 'text',
            name: {
              en: 'field1',
              'zh-CN': '字段1'
            },
            description: {
              en: 'field one',
              'zh-CN': '这是第一个字段'
            }
          },
          relation: null
        },
        {
          name: 'select1',
          schema: {
            type: 'string'
          },
          required: false,
          validation: {},
          default: null,
          display: {
            type: 'resource1',
            name: {
              en: 'field2',
              'zh-CN': '字段2'
            },
            description: {
              en: 'field two',
              'zh-CN': '这是第二个字段'
            }
          },
          relation: null
        }
      ]
    },
    {
      displayName: {
        en: 'group2',
        'zh-CN': '第二组'
      },
      relation: [
        {
          when: { name: 'abc', value: '123' },
          action: 'hide'
        }
      ],
      items: [
        {
          name: 'select2',
          schema: {
            type: 'string'
          },
          required: false,
          validation: {},
          default: null,
          display: {
            type: 'resource2',
            name: {
              en: 'field3',
              'zh-CN': '字段3'
            },
            description: {
              en: 'field one',
              'zh-CN': '这是第一个字段'
            },
            related: 'select1'
          },
          relation: null
        },
        {
          name: 'field4',
          schema: {
            type: 'string'
          },
          required: false,
          validation: {},
          default: null,
          display: {
            type: 'text',
            name: {
              en: 'field4',
              'zh-CN': '字段4'
            },
            description: {
              en: 'field two',
              'zh-CN': '这是第二个字段'
            }
          },
          relation: [
            {
              when: { name: 'select1', value: 2 },
              action: 'hide'
            }
          ]
        },
        {
          name: 'select3',
          schema: {
            type: 'string'
          },
          required: false,
          validation: {},
          default: null,
          display: {
            type: 'resource3',
            name: {
              en: 'field4',
              'zh-CN': '字段4'
            },
            description: {
              en: 'field two',
              'zh-CN': '这是第二个字段'
            },
            related: 'select2'
          },
          relation: null
        }
      ]
    }
  ];

  types: ControlMapper = {
    resource1: {
      optionsResolver: () => {
        return of([1, 2, 3]).pipe(delay(1000));
      }
    },

    resource2: {
      optionsResolver: related =>
        related
          ? of([1, 2, 3].map(item => item * Math.pow(10, related))).pipe(
              delay(1000)
            )
          : of([])
    },

    resource3: {
      optionsResolver: related =>
        related
          ? of([1, 2, 3].map(item => item + related)).pipe(delay(1000))
          : of([])
    }
  };

  groupTracker(_: number, group: GroupDefine) {
    return group.displayName.en;
  }

  fieldTracker(_: number, field: FieldDefine) {
    return field.name;
  }

  save(value) {
    console.log('save', this.ngForm.value, this.ngForm.valid);
  }
}
